# McDonald's Data Analysis in Atlanta, GA

A service that provides data in respect to the McDonalds around the metro Atlanta area. This application uses django rest to serve as web service. In order to call the Google Places API end point, you will first need an [API KEY] and store in the data_analysis/tools/config.py file

**Note: Max 60 objects from the making API Call**

## Installation
You first want to create your virtual environment
```sh
$ virtualenv env
$ source env/bin/activate
```
After, you need to install the requirements by running
```sh
$ pip install -r requirements.txt
```
followed by creating the database scheme:
```sh
$ ./manage.py migrate
```
To attain the data and store into your local database you need to go to the tools directory
```sh
$ cd tools
$ python3 import_mcdonalds_data.py
```
You will then see a message indicating process havs been completed


## Model

- *Address*: An address model will contain the information needed for this service
-- street: a string that contains the street address for the location
-- city: the name of the city
-- state: the state for the location, in this case only Georgia
-- zip_code: the postal code for the location
-- country: the country where location resides, in this case only USA
-- latitude: the latitude of the location
-- longitude: the longitude of the location
-- description: any additional information for the location
    ```
    {
        "street": "6000 N Terminal Pkwy",
        "city": "Atlanta",
        "state": "GA",
        "zip_code": "30337",
        "country": "USA",
        "latitude": "33.64071820000000",
        "longitude": "-84.42521460000000",
        "description": "Maynard H. Jackson Jr. International Terminal"
    }
    ```

## Endpoints
- /mcdonalds/location[?query_params]
    Return a JSON list of all the addresses. Optionally filter by add query paramater:
    - zip_code=\<zip_code\>: Return only the address that contains (when input 3 integers) or match the zip code (5 integers)

- /mcdonalds/locations/report/
    Return a JSON list containing all of the distinct zip codes with the associated count
    ```
    [
        {
            "30329": 3
        },
        {
            "30344": 2
        },
        {
            "30331": 1
        },
        ....
    ]
    ```

- mcdonalds/locations/centroid[?query_params]: Return a JSON list containing the latitude and longitude for the centroid given the restaurants in the specified zip code
    - zip_code=\<zip_code\>: Return only the address that contains (when input 3 integers) or match the zip code (5 integers)
    ```
    {
        "longitude": -84.3278068,
        "latitude": 33.83975186666667
    }
    ```

   [API KEY]: <https://developers.google.com/places/web-service/get-api-key>
