from django.conf.urls import url, include
from ..views.address import AddressViewSet, ReportViewSet, CentroidView


urlpatterns = [
    url(r'^mcdonalds/locations/report/', ReportViewSet.as_view()),
    url(r'^mcdonalds/locations/centroid', CentroidView.as_view()),
    url(r'^mcdonalds/locations', AddressViewSet.as_view({'get': 'list',
                                               'post': 'create'})),
]

