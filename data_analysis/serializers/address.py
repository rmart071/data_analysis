from rest_framework import serializers

from ..models.address import Address


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ('street', 'city', 'state', 'zip_code', 'country','latitude', 'longitude', 'description')

    def create(self, validated_data):
        return Address.objects.create(**validated_data)
