#!/usr/bin/env python
from googleplaces import GooglePlaces, types, lang


import argparse
import requests

from config import API_KEY


google_places = GooglePlaces(API_KEY)

query_result = google_places.text_search(query='McDonalds+in+Atlanta,GA')

parser = argparse.ArgumentParser()
parser.add_argument('--url', dest='url', action='store', default='http://127.0.0.1:8000/mcdonalds/locations/',
                    help='URL to add the locations for McDonalds in Atlanta')
args = parser.parse_args()


def compose_address(address_string, location):
    address_list = [x.strip() for x in address_string.split(', ')]
    description = None
    if len(address_list) > 4:
        description = address_list.pop(0)
    state_zip = address_list[2].split()
    return({
        'street': address_list[0],
        'city': address_list[1],
        'state': state_zip[0],
        'zip_code': state_zip[1],
        'country': address_list[3],
        'latitude': float(location['lat']),
        'longitude': float(location['lng']),
        'description': description
    })


#
while True:
    for place in query_result.places:
        location = place.geo_location
        place.get_details()
        address = compose_address(place.details['formatted_address'], location)

        res = requests.post(args.url, json=address)
        if res.status_code == 201:
            print('New McDonald\'s Location {} was added'.format(address['street']))
        else:
            print('Error! Location was not added. Status code {} returned'.format(res.status_code))

    if query_result.has_next_page_token:
        query_result = google_places.text_search(
            pagetoken=query_result.next_page_token)
    else:
        break

print('Process is now complete')
