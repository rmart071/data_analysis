from django.db import models


class Address(models.Model):
    street = models.CharField(max_length=128, null=False)
    city = models.CharField(max_length=64, null=False)
    state = models.CharField(max_length=2, null=False)
    zip_code = models.CharField(max_length=5, null=False)
    country = models.CharField(max_length=5, null=False)
    latitude = models.DecimalField(max_digits=17, decimal_places=14, null=False, default=0.0)
    longitude = models.DecimalField(max_digits=17, decimal_places=14, null=False, default=0.0)
    description = models.CharField(max_length=512, null=True)


    class Meta:
        db_table = 'address'
        unique_together = (('street', 'zip_code'),)

