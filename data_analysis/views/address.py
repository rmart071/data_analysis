from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

import pandas as pd

from ..models.address import Address
from ..serializers.address import AddressSerializer


def find_centroid(df):
    return {
        'latitude': pd.to_numeric(df['latitude']).mean(),
        'longitude': pd.to_numeric(df['longitude']).mean()
    }


class AddressViewSet(viewsets.ModelViewSet):
    serializer_class = AddressSerializer
    lookup_field = 'zip_code'

    def get_queryset(self):
        queryset = Address.objects.all()
        zip_code = self.request.query_params.get('zip_code', None)
        if zip_code is not None:
            if len(zip_code) == 3:
                queryset = queryset.filter(zip_code__contains=zip_code)
            elif len(zip_code) == 5:
                queryset = queryset.filter(zip_code=zip_code)
            else:
                print('Invalid zip_code ')
                queryset = None
        return queryset


class ReportViewSet(APIView):
    def get(self, request):
        queryset = Address.objects.all()
        zip_list = []
        serializer = AddressSerializer(queryset, many=True)
        df = pd.DataFrame.from_dict(serializer.data)
        df_zip_count = df['zip_code'].value_counts()
        for key, value in df_zip_count.iteritems():
            zip_list.append({
                key: value
            })

        return Response(zip_list)


class CentroidView(APIView):
    def get(self, request):
        queryset = Address.objects.all()
        zip_code = self.request.query_params.get('zip_code', None)

        if zip_code is not None:
            if len(zip_code) == 3:
                queryset = queryset.filter(zip_code__contains=zip_code)
            elif len(zip_code) == 5:
                queryset = queryset.filter(zip_code=zip_code)
            else:
                print('Invalid zip_code ')
                queryset = None

        serializer = AddressSerializer(queryset, many=True)
        df = pd.DataFrame.from_dict(serializer.data)
        location = {}
        if len(df) > 0:
            df_locations = pd.DataFrame({'latitude': df['latitude'], 'longitude': df['longitude']})
            location = find_centroid(df_locations)

        return Response(location)
